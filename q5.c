#include<stdio.h>
int main()
{
	int A=10, B=15,AND,XOR,NOT,lshift,rshift;
	// Bitwise AND oprator
	AND= A & B;
	printf("A & B = %d\n",AND);

	//Bitwise XOR operator
	XOR=A ^ B;
	printf("A ^ B = %d\n",XOR);

	//Bitwise NOT operator
	NOT=~A;
	printf("~A = %d\n",NOT);

	//Left shift operator
	lshift= A<<3;
	printf("A<<3 = %d\n",lshift);

	//Right shift operator
	rshift= B>>3;
	printf("B>>3 = %d\n",rshift);
	return 0;
}
